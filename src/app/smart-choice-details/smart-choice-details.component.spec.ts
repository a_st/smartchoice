import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SmartChoiceDetailsComponent} from './smart-choice-details.component';
import {TestingModule} from '../shared/testing/testing.module';
import {of} from 'rxjs';
import {SmartChoice} from '../shared/models/smart-choice';
import {Group} from '../shared/models/group';
import {Person} from '../shared/models/person';
import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {ActivatedRoute} from '@angular/router';

fdescribe('SmartChoiceDetailsComponent', () => {
  let component: SmartChoiceDetailsComponent;
  let fixture: ComponentFixture<SmartChoiceDetailsComponent>;

  const smartChoices = [];

  beforeEach(async(() => {

    const smartChoice = new SmartChoice();
    smartChoice.title = 'TestSmartChoice';
    smartChoice.groups.push(new Group());
    smartChoice.groups.push(new Group());
    smartChoice.persons.push(new Person());

    smartChoices.push(smartChoice);

    const smartChoiceService = jasmine.createSpyObj('SmartChoiceService', ['getSmartChoices']);
    smartChoiceService.getSmartChoices.and.returnValue(of(smartChoices));

    TestBed.configureTestingModule({
      declarations: [ SmartChoiceDetailsComponent ],
      imports: [TestingModule],
      providers: [
        {provide: SmartChoiceService, useValue: smartChoiceService},
        { provide: ActivatedRoute, useValue: { params: of({index: 0}) } }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartChoiceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have loaded the smartChoice', () => {
    expect(component.smartChoice).toBe(smartChoices[0]);
    expect(component.smartChoiceIndex).toBe(0);
  });
});

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {SmartChoice} from '../shared/models/smart-choice';
import {Title} from '@angular/platform-browser';
import {MatDialog, MatSnackBar} from '@angular/material';
import {DialogService} from '../shared/services/dialog.service';
import {RenameDialogComponent} from '../shared/dialogs/rename-dialog/rename-dialog.component';

@Component({
  selector: 'app-smart-choice-details',
  templateUrl: './smart-choice-details.component.html',
  styleUrls: ['./smart-choice-details.component.scss']
})
export class SmartChoiceDetailsComponent implements OnInit {

  public smartChoice: SmartChoice;
  public smartChoiceIndex: number;
  preferenceLink = 'https://notarealurl.com';

  constructor(private route: ActivatedRoute, private router: Router, private smartChoiceService: SmartChoiceService,
              private titleService: Title, private snackBar: MatSnackBar, private dialogService: DialogService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.smartChoiceIndex = params.index;
      this.smartChoiceService.getSmartChoices().subscribe(choices => {
        this.smartChoice = choices[this.smartChoiceIndex];
        this.titleService.setTitle('SmartChoice – ' + this.smartChoice.title);
      });
    });
  }

  deleteSmartChoice() {
    this.dialogService.confirm('Are you sure you want to delete this item?', 'Delete').subscribe(value => {
      if (value) {
        this.smartChoiceService.deleteSmartChoice(this.smartChoice);
        this.router.navigate(['/']);
      }
    });
  }

  renameSmartChoice() {
    this.dialog.open(RenameDialogComponent, {width: '400px', data: {name: this.smartChoice.title}}).afterClosed().subscribe(value => {
      if (value) {
        this.smartChoice.title = value;
        this.smartChoiceService.saveSmartChoice(this.smartChoice);
      }
    });

  }
}

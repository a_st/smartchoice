import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartChoicePersonListComponent } from './smart-choice-person-list.component';
import {TestingModule} from '../shared/testing/testing.module';

describe('SmartChoicePersonListComponent', () => {
  let component: SmartChoicePersonListComponent;
  let fixture: ComponentFixture<SmartChoicePersonListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartChoicePersonListComponent ],
      imports: [ TestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartChoicePersonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

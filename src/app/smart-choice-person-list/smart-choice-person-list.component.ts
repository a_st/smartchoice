import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {v4 as uuid} from 'uuid';

import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {SmartChoice} from '../shared/models/smart-choice';
import {Person} from '../shared/models/person';
import {MatDialog, MatSnackBar} from '@angular/material';
import {RenameDialogComponent} from '../shared/dialogs/rename-dialog/rename-dialog.component';

@Component({
  selector: 'app-smart-choice-person-list',
  templateUrl: './smart-choice-person-list.component.html',
  styleUrls: ['./smart-choice-person-list.component.scss']
})
export class SmartChoicePersonListComponent implements OnInit {

  public smartChoice: SmartChoice;
  public smartChoiceIndex: number;
  public newPersonsName: string;
  public allSmartChoices: SmartChoice[];

  constructor(private route: ActivatedRoute, private smartChoiceService: SmartChoiceService, private snackBar: MatSnackBar, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.smartChoiceIndex = params.index;
      this.smartChoiceService.getSmartChoices().subscribe(choices => {
        this.allSmartChoices = choices;
        this.smartChoice = choices[this.smartChoiceIndex];
      });
    });
  }

  keyPressed(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.addNewPerson();
    }
  }

  public addNewPerson() {
    if (this.newPersonsName) {
      const person = new Person();
      person.name = this.newPersonsName;
      person.uuid = uuid();

      if (this.smartChoice.persons.findIndex(p => p.name === person.name) === -1) {
        this.smartChoice.persons.push(person);

        this.smartChoiceService.saveSmartChoice(this.smartChoice);
        this.newPersonsName = '';
      } else {
        this.snackBar.open('That group already exists!', null, {duration: 1000});
      }
    }
  }

  removePerson(index: number) {
    const deletedPerson = this.smartChoice.persons.splice(index, 1)[0];
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
    const snackBarRef = this.snackBar.open('Deleted: \"' + deletedPerson.name + '\"', 'Undo', {duration: 10000});
    snackBarRef.onAction().subscribe(() => {
      this.smartChoice.persons.splice(index, 0, deletedPerson);

      this.smartChoiceService.saveSmartChoice(this.smartChoice);
    });
  }

  editPerson(index: number) {
    const dialogRef = this.dialog.open(RenameDialogComponent, {
      width: '250px',
      data: {name: this.smartChoice.persons[index].name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.smartChoice.persons[index].name = result;
        this.smartChoiceService.saveSmartChoice(this.smartChoice);
      }
    });
  }

  importPersonsFromSmartChoice(sc: SmartChoice) {
    sc.persons
      .map(person => {
        person.preferredGroups = [];
        person.uuid = uuid();
        return person;
      })
      .filter(person => this.smartChoice.persons.findIndex(p => p.name === person.name) === -1)
      .forEach(person => this.smartChoice.persons.unshift(person));
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
  }

  getAllSmartChoicesExceptThis() {
    return this.allSmartChoices.filter(value => value !== this.smartChoice);
  }
}

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {v4 as uuid} from 'uuid';

import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {SmartChoice} from '../shared/models/smart-choice';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Group} from '../shared/models/group';
import {GroupEditDialogComponent} from '../shared/dialogs/group-edit-dialog/group-edit-dialog.component';

@Component({
  selector: 'app-smart-choice-group-list',
  templateUrl: './smart-choice-group-list.component.html',
  styleUrls: ['./smart-choice-group-list.component.scss']
})
export class SmartChoiceGroupListComponent implements OnInit {

  @ViewChild('groupNameInput', null) groupNameInput: ElementRef;

  public smartChoice: SmartChoice;
  public smartChoiceIndex: number;
  public newGroupName: string;

  public newGroupCapacity: number;
  public allSmartChoices: SmartChoice[];

  constructor(private route: ActivatedRoute, private smartChoiceService: SmartChoiceService, private snackBar: MatSnackBar, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.smartChoiceIndex = params.index;
      this.smartChoiceService.getSmartChoices().subscribe(choices => {
        this.allSmartChoices = choices;
        this.smartChoice = choices[this.smartChoiceIndex];
      });
    });
  }

  keyPressed(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.addNewGroup();
    }
  }

  public addNewGroup() {
    if (this.newGroupName && this.newGroupCapacity) {
      this.newGroupCapacity = Math.abs(this.newGroupCapacity);
      this.newGroupCapacity = Math.floor(this.newGroupCapacity);

      const group = new Group();
      group.name = this.newGroupName;
      group.capacity = this.newGroupCapacity;
      group.uuid = uuid();

      if (this.smartChoice.groups.findIndex(g => g.name === group.name) === -1) {
        this.smartChoice.groups.push(group);

        this.smartChoiceService.saveSmartChoice(this.smartChoice);
        this.newGroupName = '';
        this.groupNameInput.nativeElement.focus();
      } else {
        this.snackBar.open('That group already exists!', null, {duration: 1000});
      }
    }
  }

  removeGroup(index: number) {
    const deletedGroup = this.smartChoice.groups.splice(index, 1)[0];
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
    const snackBarRef = this.snackBar.open('Deleted: \"' + deletedGroup.name + '\"', 'Undo', {duration: 10000});
    snackBarRef.onAction().subscribe(() => {
      this.smartChoice.groups.splice(index, 0, deletedGroup);

      this.smartChoiceService.saveSmartChoice(this.smartChoice);
    });
  }

  editGroup(index: number) {
    const dialogRef = this.dialog.open(GroupEditDialogComponent, {
      width: '600x',
      data: {name: this.smartChoice.groups[index].name, capacity: this.smartChoice.groups[index].capacity}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.smartChoice.groups[index].name = result.name;
        this.smartChoice.groups[index].capacity = result.capacity;
        this.smartChoiceService.saveSmartChoice(this.smartChoice);
      }
    });
  }

  importGroupsFromSmartChoice(sc: SmartChoice) {
    sc.groups
      .map(group => {
        group.uuid = uuid();
        return group;
      })
      .filter(group => this.smartChoice.groups.findIndex(g => g.name === group.name) === -1)
      .forEach(group => this.smartChoice.groups.unshift(group));
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
  }

  getAllSmartChoicesExceptThis() {
    return this.allSmartChoices.filter(value => value !== this.smartChoice);
  }
}

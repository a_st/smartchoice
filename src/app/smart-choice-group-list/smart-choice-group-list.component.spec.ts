import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartChoiceGroupListComponent } from './smart-choice-group-list.component';
import {TestingModule} from '../shared/testing/testing.module';

describe('SmartChoiceGroupListComponent', () => {
  let component: SmartChoiceGroupListComponent;
  let fixture: ComponentFixture<SmartChoiceGroupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartChoiceGroupListComponent ],
      imports: [ TestingModule ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartChoiceGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

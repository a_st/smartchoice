import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SmartChoiceListComponent} from './smart-choice-list.component';
import {TestingModule} from '../shared/testing/testing.module';
import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {of} from 'rxjs';
import {SmartChoice} from '../shared/models/smart-choice';
import {Group} from '../shared/models/group';
import {Person} from '../shared/models/person';
import {Router} from '@angular/router';

describe('SmartChoiceListComponent', () => {
  let component: SmartChoiceListComponent;
  let fixture: ComponentFixture<SmartChoiceListComponent>;
  let compiled: HTMLElement;
  const smartChoices = [];

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {

    const smartChoice = new SmartChoice();
    smartChoice.title = 'TestSmartChoice';
    smartChoice.groups.push(new Group());
    smartChoice.groups.push(new Group());
    smartChoice.persons.push(new Person());

    smartChoices.push(smartChoice);

    const smartChoiceService = jasmine.createSpyObj('SmartChoiceService', ['getSmartChoices']);
    smartChoiceService.getSmartChoices.and.returnValue(of(smartChoices));

    TestBed.configureTestingModule({
      declarations: [SmartChoiceListComponent],
      imports: [TestingModule],
      providers: [
        {provide: SmartChoiceService, useValue: smartChoiceService},
        {provide: Router, useValue: routerSpy}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartChoiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain all smartChoices', () => {
    expect(component.smartChoices === smartChoices).toBeTruthy();
  });

  it('should display SmartChoice with title TestSmartChoice', () => {
    expect(compiled.querySelector('.title').textContent).toBe('TestSmartChoice');
  });

  it('should display correct group and person count', () => {
    expect(compiled.querySelector('.person-count').textContent).toBe('1');
    expect(compiled.querySelector('.group-count').textContent).toBe('2');
  });

  it('should load smartChoiceDetailComponent via router with correct url when clicking on a smartChoiceItem', () => {
    component.viewSmartChoiceItem(0);

    // args passed to router.navigateByUrl() spy
    const spy = routerSpy.navigate as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];

    expect(navArgs[0]).toBe('smartChoicesDetails/0');
  });
});

import {Component, OnInit} from '@angular/core';
import {SmartChoice} from '../shared/models/smart-choice';
import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MatDialog} from '@angular/material';
import {NewSmartChoiceDialogComponent} from '../shared/dialogs/new-smart-choice-dialog/new-smart-choice-dialog.component';

@Component({
  selector: 'app-smart-choice-list',
  templateUrl: './smart-choice-list.component.html',
  styleUrls: ['./smart-choice-list.component.scss']
})
export class SmartChoiceListComponent implements OnInit {

  public smartChoices: SmartChoice[];
  exportedString: string;
  showImport = false;
  importedString: string;

  constructor(private smartChoiceService: SmartChoiceService, private router: Router, private titleService: Title, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.titleService.setTitle('SmartChoice');
    this.smartChoiceService.getSmartChoices().subscribe(choices => {
      this.smartChoices = choices;
    });
  }

  viewSmartChoiceItem(index: number) {
    this.router.navigate(['smartChoicesDetails/' + index]);
  }

  newSmartChoice() {
    const dialogRef = this.dialog.open(NewSmartChoiceDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const smartChoice = new SmartChoice();
        smartChoice.title = result;
        this.smartChoiceService.saveSmartChoice(smartChoice);
        this.smartChoices.unshift(smartChoice);
      }
    });
  }

  export() {
    this.exportedString = this.smartChoiceService.getSmartChoiceBASE64(this.smartChoices);
    this.showImport = false;
    this.importedString = '';
  }

  import() {
    this.showImport = true;
    this.exportedString = '';
    this.importedString = '';
  }

  finishImport() {
    this.smartChoiceService.getSmartChoicesFromBASE64(this.importedString).subscribe(choices => {
      this.smartChoices.push(...choices);
      this.smartChoiceService.saveSmartChoices(this.smartChoices);
      this.importedString = '';
      this.showImport = false;
    });
  }
}

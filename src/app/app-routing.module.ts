import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SmartChoiceListComponent} from './smart-choice-list/smart-choice-list.component';
import {SmartChoiceDetailsComponent} from './smart-choice-details/smart-choice-details.component';
import {SmartChoicePersonListComponent} from './smart-choice-person-list/smart-choice-person-list.component';
import {SmartChoiceGroupListComponent} from './smart-choice-group-list/smart-choice-group-list.component';
import {SmartChoiceManualPreferencesComponent} from './smart-choice-manual-preferences/smart-choice-manual-preferences.component';
import {SmartChoiceDivisionComponent} from './smart-choice-division/smart-choice-division.component';

export const routes: Routes = [
  {path: 'smartChoices', component: SmartChoiceListComponent},
  {path: 'smartChoicesDetails/:index', component: SmartChoiceDetailsComponent},
  {path: 'smartChoicePersons/:index', component: SmartChoicePersonListComponent},
  {path: 'smartChoiceGroups/:index', component: SmartChoiceGroupListComponent},
  {path: 'smartChoiceManualPreferences/:index', component: SmartChoiceManualPreferencesComponent},
  {path: 'smartChoiceDivision/:index', component: SmartChoiceDivisionComponent},
  {path: '', redirectTo: '/smartChoices', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

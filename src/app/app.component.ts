import {Component} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SmartChoice';

  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon(
      'app_logo',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/app_logo.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'app_logo_inverted',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/app_logo_inverted.svg')
    );
  }
}

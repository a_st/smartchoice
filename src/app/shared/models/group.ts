export class Group {
  public uuid: string;
  public name: string;
  public capacity: number;
}

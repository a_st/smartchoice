import {Group} from './group';

export class Person {
  public uuid: string;
  public name: string;
  public preferredGroups: Group[] = [];
}

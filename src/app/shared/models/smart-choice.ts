import {Group} from './group';
import {Person} from './person';

export class SmartChoice {

  public groups: Group[] = [];
  public persons: Person[] = [];
  public title = '';
  // tslint:disable-next-line:variable-name
  private _created: number;

  constructor() {
    this._created = Date.now();
  }

  get created(): number {
    return this._created;
  }

  public copyInto(jsonObject: any) {
    this._created = jsonObject._created;
    this.title = jsonObject.title;
    this.groups = jsonObject.groups;
    this.persons = jsonObject.persons;
  }

  calculateCapacity(): number {
    if (this.groups.length === 0) {
      return 0;
    }
    return this.groups
      .map(group => group.capacity)
      .reduce((previousValue, currentValue) => previousValue + currentValue);
  }

  isDivisionPossible(): boolean {
    if (this.persons.length === 0 || this.groups.length === 0) {
      return false;
    }
    return this.persons.length <= this.calculateCapacity();
  }
}

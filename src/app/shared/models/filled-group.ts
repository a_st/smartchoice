import {Group} from './group';
import {Person} from './person';

export class FilledGroup {

  public group: Group;
  public persons: Person[] = [];

  /**
   * Will be set by SmartChoiceDivision service increase the capacity by this number and re-run = the results will be better
   */
  public increaseCapacityNotice = 0;

  /**
   * Just for the template
   */
  public increaseCapacity = 1;
}

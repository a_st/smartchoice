import {FilledGroup} from './filled-group';

export class DivisionResult {

  public filledGroups: FilledGroup[];
  public satisfaction: number;

}

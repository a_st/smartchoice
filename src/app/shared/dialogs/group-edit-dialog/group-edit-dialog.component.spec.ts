import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GroupEditDialogComponent} from './group-edit-dialog.component';
import {TestingModule} from '../../testing/testing.module';

describe('GroupEditDialogComponent', () => {
  let component: GroupEditDialogComponent;
  let fixture: ComponentFixture<GroupEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupEditDialogComponent],
      imports: [TestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

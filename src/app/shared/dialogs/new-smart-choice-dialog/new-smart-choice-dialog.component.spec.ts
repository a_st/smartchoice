import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewSmartChoiceDialogComponent} from './new-smart-choice-dialog.component';
import {TestingModule} from '../../testing/testing.module';

describe('NewSmartChoiceDialogComponent', () => {
  let component: NewSmartChoiceDialogComponent;
  let fixture: ComponentFixture<NewSmartChoiceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewSmartChoiceDialogComponent],
      imports: [TestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSmartChoiceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

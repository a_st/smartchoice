import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-new-smart-choice-dialog',
  templateUrl: './new-smart-choice-dialog.component.html',
  styleUrls: ['./new-smart-choice-dialog.component.scss']
})
export class NewSmartChoiceDialogComponent {
  public name = '';

  constructor(
    public dialogRef: MatDialogRef<NewSmartChoiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  keyPressed($event: KeyboardEvent) {
    if ($event.key === 'Enter') {
      this.dialogRef.close(this.name);
    }
  }
}

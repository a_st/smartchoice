import { TestBed } from '@angular/core/testing';

import { SmartChoiceDivisionService } from './smart-choice-division.service';

describe('SmartChoiceDivisionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SmartChoiceDivisionService = TestBed.get(SmartChoiceDivisionService);
    expect(service).toBeTruthy();
  });
});

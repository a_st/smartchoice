import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {SmartChoice} from '../models/smart-choice';
import {group} from '@angular/animations';

@Injectable({
  providedIn: 'root'
})
export class SmartChoiceService {

  constructor() {
  }

  public getSmartChoices(): Observable<SmartChoice[]> {
    return this.getSmartChoicesFromJSONString(localStorage.getItem('smartChoices'));
  }

  private getSmartChoicesFromJSONString(smartChoicesString: string) {
    if (!smartChoicesString) {
      smartChoicesString = '[]';
    }
    let smartChoices = JSON.parse(smartChoicesString) as SmartChoice[];
    smartChoices = smartChoices.map(smartChoice => {
      const s = new SmartChoice();
      s.copyInto(smartChoice);
      return s;
    });
    return of(smartChoices);
  }

  public deleteSmartChoice(smartChoice: SmartChoice) {
    this.getSmartChoices().subscribe(choices => {
      const cI = choices.findIndex(c => c.created === smartChoice.created);
      if (cI !== -1) {
        choices.splice(cI, 1);
      }
      this.saveSmartChoices(choices);
    });
  }

  public saveSmartChoice(smartChoice: SmartChoice) {
    this.getSmartChoices().subscribe(choices => {
      const cI = choices.findIndex(c => c.created === smartChoice.created);
      if (cI === -1) {
        // new choice
        choices.unshift(smartChoice);
      } else {
        // overwrite old one
        choices[cI] = smartChoice;
      }
      this.saveSmartChoices(choices);
    });
  }

  public saveSmartChoices(smartChoices: SmartChoice[]) {
    const smartChoicesString = this.getSmartChoicesString(smartChoices);
    localStorage.setItem('smartChoices', smartChoicesString);
  }

  private getSmartChoicesString(smartChoices: SmartChoice[]) {
    // make sure all uuid's match the correct names
    smartChoices.forEach(smartChoice => {
      smartChoice.persons.forEach(person => {
        person.preferredGroups.forEach((pGroup, index) => {
          const updatedGroup = smartChoice.groups.find(g => g.uuid === pGroup.uuid);
          if (updatedGroup) {
            // update old Group
            person.preferredGroups.splice(index, 1, updatedGroup);
          } else {
            // delete old Group
            person.preferredGroups.splice(index, 1);
          }
        });
      });
    });

    return JSON.stringify(smartChoices);
  }

  getSmartChoiceBASE64(smartChoices: SmartChoice[]) {
    return btoa(this.getSmartChoicesString(smartChoices));
  }

  getSmartChoicesFromBASE64(base64string: string) {
    return this.getSmartChoicesFromJSONString(atob(base64string));
  }
}

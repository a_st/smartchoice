import { TestBed } from '@angular/core/testing';

import { FilledGroupService } from './filled-group.service';

describe('FilledGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilledGroupService = TestBed.get(FilledGroupService);
    expect(service).toBeTruthy();
  });
});

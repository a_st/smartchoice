import {Injectable} from '@angular/core';
import {SmartChoice} from '../models/smart-choice';
import {FilledGroup} from '../models/filled-group';
import {Group} from '../models/group';
import {Person} from '../models/person';
import {FilledGroupService} from './filled-group.service';
import {DivisionResult} from '../models/division-result';

@Injectable({
  providedIn: 'root'
})
export class SmartChoiceDivisionService {

  constructor(private filledGroupService: FilledGroupService) {
  }

  /**
   *
   * @param maxDissatisfaction should be something like:  number of votes +1
   */
  divideUsersOntoGroupsRepeat(smartChoice: SmartChoice, times: number): DivisionResult {
    let divisionResult = this.divideUsersOntoGroups(smartChoice);
    let bestSatisfaction = divisionResult.satisfaction;
    for (let i = 1; i < times; i++) {
      const dR = this.divideUsersOntoGroups(smartChoice);
      if (dR.satisfaction < bestSatisfaction) {
        divisionResult = dR;
        bestSatisfaction = dR.satisfaction;
      }
    }
    return divisionResult;
  }

  divideUsersOntoGroups(smartChoice: SmartChoice): DivisionResult {
    // step1: put all persons into their first wish
    let filledGroups = this.fulfillAllFirstWishes(smartChoice);
    // step2: shuffle all persons so its fair
    filledGroups.map(filledGroup => {
      filledGroup.persons = this.shuffleArray(filledGroup.persons);
      return filledGroup;
    });
    // step3: remove person from groups which are full and put them into their other preferences
    filledGroups = this.dividePersonsOfFullGroupsToOthers(filledGroups);

    // step4: fill persons where no wish has been met
    filledGroups = this.divideUndividedPersons(filledGroups, smartChoice);

    // step5: try to switch pairs of really not satisfied ones with very satisfied ones
    for (let i = 0; i < 1; i++) {
      filledGroups = this.switchSatisfiedWithNotSatisfiedOnes(filledGroups);
    }
    const divisionResult = new DivisionResult();
    divisionResult.filledGroups = filledGroups;
    divisionResult.satisfaction = this.filledGroupService.calculateDissatisfaction(divisionResult.filledGroups);

    return divisionResult;
  }

  /**
   * Put every person into its first wish (first preference)
   * does NOT check for capacity
   */
  private fulfillAllFirstWishes(smartChoice: SmartChoice): FilledGroup[] {
    const filledGroups = smartChoice.groups.map(group => {
      const filledGroup = new FilledGroup();
      filledGroup.group = group;
      return filledGroup;
    });

    smartChoice.persons.forEach(person => {
      if (person.preferredGroups.length > 0) {
        this.putPersonIntoGroup(person, person.preferredGroups[0], filledGroups);
      }
    });

    return filledGroups;
  }

  private dividePersonsOfFullGroupsToOthers(filledGroups: FilledGroup[]) {
    filledGroups.forEach(filledGroup => {
      if (this.isFilledGroupFull(filledGroup)) {
        // capacity is too small
        const persons = filledGroup.persons.splice(filledGroup.group.capacity);
        // leaving a notice that user might increase the capacity
        filledGroup.increaseCapacityNotice = persons.length;
        filledGroup.increaseCapacity = persons.length;

        // put these persons into other groups
        persons.forEach(person => {
          let personIsPushed = false;
          person.preferredGroups.slice(1).forEach(group => {
            if (!personIsPushed) {
              // check if capacity is enough then put it on there
              const g = this.getFilledGroupForGroup(group, filledGroups);
              if (!this.isFilledGroupFull(g)) {
                g.persons.push(person);
                personIsPushed = true;
              }
            }
          });
        });
      }
    });
    return filledGroups;
  }

  private divideUndividedPersons(filledGroups: FilledGroup[], smartChoice: SmartChoice) {
    smartChoice.persons.forEach(person => {
      const index = filledGroups.findIndex(filledGroup =>
        filledGroup.persons.findIndex(p => p.uuid === person.uuid) !== -1
      );
      if (index === -1) {
        // put this person into a not full group
        filledGroups.filter(filledGroup => !this.isFilledGroupFull(filledGroup))[0]
          .persons.push(person);
      }
    });
    return filledGroups;
  }

  private switchSatisfiedWithNotSatisfiedOnes(filledGroups: FilledGroup[]): FilledGroup[] {
    const persons = filledGroups
      .map(filledGroup => {
        return filledGroup.persons
          .map(person => {
              const index = person.preferredGroups.findIndex(group => group.uuid === filledGroup.group.uuid);
              let satisfaction = index;
              if (index === -1) {
                satisfaction = person.preferredGroups.length;
              }
              return {person, satisfaction, group: filledGroup.group};
            }
          );
      })
      .reduce((previousValue, currentValue) => previousValue.concat(currentValue));

    persons.sort((a, b) => b.satisfaction - a.satisfaction);
    // it doesn't make sense to grab also those who got their second wish fulfilled because a switch won't make the group happier
    const firstIndexWithAlmostPerfectSatisfaction = persons.findIndex(person => person.satisfaction === 1);

    const unhappyPersons = persons.slice(0, firstIndexWithAlmostPerfectSatisfaction);
    const happyPersons = persons.slice(firstIndexWithAlmostPerfectSatisfaction);

    // try to switch unhappyPersons with happyPersons to make them happier ;)
    unhappyPersons.forEach(unhappyPerson => {
      happyPersons.forEach(happyPerson => {
        if (unhappyPerson.group.uuid !== happyPerson.group.uuid) { // only proceed if a change makes an actual difference
          const satisfactionBeforeChange = unhappyPerson.satisfaction + happyPerson.satisfaction;
          const index = unhappyPerson.person.preferredGroups.findIndex(group => group.uuid === happyPerson.group.uuid);
          if (index !== -1) {
            // good: unhappyPerson wants to get into the happyPerson's group
            // check if happyPerson wants to go into unhappy's group
            const index2 = happyPerson.person.preferredGroups.findIndex(group => group.uuid === unhappyPerson.group.uuid);
            if (index2 !== -1) {
              // we got a match, now we just need to check if this switch will actually increase our satisfaction
              const newSatisfaction = index + index2;
              if (newSatisfaction < satisfactionBeforeChange) {
                // only change if better
                const happyPersonsFilledGroup = this.getFilledGroupForGroup(happyPerson.group, filledGroups);
                const happyPersonIndex = happyPersonsFilledGroup.persons.findIndex(person => person.uuid === happyPerson.person.uuid);
                // delete happyPersonFromOldGroup and fill in the new unhappyPerson
                happyPersonsFilledGroup.persons.splice(happyPersonIndex, 1, unhappyPerson.person);
                // now put the happyPerson into  the unhappyPerson's group
                const unhappyPersonsFilledGroup = this.getFilledGroupForGroup(unhappyPerson.group, filledGroups);
                const unhappyPersonIndex = unhappyPersonsFilledGroup.persons.findIndex(person => person.uuid === unhappyPerson.person.uuid);
                unhappyPersonsFilledGroup.persons.splice(unhappyPersonIndex, 1, happyPerson.person);

                happyPerson.group = unhappyPersonsFilledGroup.group;
                unhappyPerson.group = happyPersonsFilledGroup.group;
              }
            }
          }
        }
      });
    });
    return filledGroups;
  }

  private isFilledGroupFull(filledGroup: FilledGroup) {
    return filledGroup.persons.length >= filledGroup.group.capacity;
  }

  private getFilledGroupForGroup(group: Group, filledGroups: FilledGroup[]): FilledGroup {
    return filledGroups.find(filledGroup => filledGroup.group.uuid === group.uuid);
  }

  /**
   * Does not check whether the person is already in one group
   */
  private putPersonIntoGroup(person: Person, group: Group, filledGroups: FilledGroup[]) {
    const g = this.getFilledGroupForGroup(group, filledGroups);
    if (g) {
      g.persons.push(person);
    }
  }

  /**
   * Shuffles array in place. ES6 version
   * @param a items An array containing the items.
   */
  private shuffleArray(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }
}

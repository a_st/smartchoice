import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../dialogs/confirmation-dialog/confirmation-dialog.component';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) {
  }

  public confirm(message: string, title: string): Observable<boolean> {
    return this.dialog.open(ConfirmationDialogComponent, {width: '500px', data: {message: message, title: title}}).afterClosed();
  }
}

import {Injectable} from '@angular/core';
import {FilledGroup} from '../models/filled-group';
import {group} from '@angular/animations';

@Injectable({
  providedIn: 'root'
})
export class FilledGroupService {

  constructor() {
  }

  /**
   * The higher the worse the match
   */
  calculateDissatisfaction(filledGroups: FilledGroup[]): number {
    let satisfaction = 0;
    filledGroups.forEach(filledGroup => {
      filledGroup.persons.forEach(person => {

        let index = person.preferredGroups.findIndex(g => g.uuid === filledGroup.group.uuid);
        if (index === -1) {
          index = person.preferredGroups.length;
        }
        satisfaction += index;
      });
    });
    return satisfaction;
  }

}

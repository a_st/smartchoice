import { TestBed } from '@angular/core/testing';

import { SmartChoiceService } from './smart-choice.service';

describe('SmartChoiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SmartChoiceService = TestBed.get(SmartChoiceService);
    expect(service).toBeTruthy();
  });
});

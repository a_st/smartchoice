import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatListModule} from '@angular/material/list';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {SmartChoiceListComponent} from './smart-choice-list/smart-choice-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatChipsModule, MatDialogModule, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatMenuModule, MatProgressBarModule,
  MatRippleModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule,
  MatToolbarModule, MatTooltipModule
} from '@angular/material';
import {SmartChoiceDetailsComponent} from './smart-choice-details/smart-choice-details.component';
import {HttpClientModule} from '@angular/common/http';
import {SmartChoicePersonListComponent} from './smart-choice-person-list/smart-choice-person-list.component';
import {FormsModule} from '@angular/forms';
import {RenameDialogComponent} from './shared/dialogs/rename-dialog/rename-dialog.component';
import {SmartChoiceGroupListComponent} from './smart-choice-group-list/smart-choice-group-list.component';
import {GroupEditDialogComponent} from './shared/dialogs/group-edit-dialog/group-edit-dialog.component';
import {SmartChoiceManualPreferencesComponent} from './smart-choice-manual-preferences/smart-choice-manual-preferences.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {SmartChoiceDivisionComponent} from './smart-choice-division/smart-choice-division.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { NewSmartChoiceDialogComponent } from './shared/dialogs/new-smart-choice-dialog/new-smart-choice-dialog.component';
import {ConfirmationDialogComponent} from './shared/dialogs/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  entryComponents: [
    RenameDialogComponent,
    GroupEditDialogComponent,
    NewSmartChoiceDialogComponent,
    ConfirmationDialogComponent
  ],
  declarations: [
    AppComponent,
    SmartChoiceListComponent,
    SmartChoiceDetailsComponent,
    SmartChoicePersonListComponent,
    RenameDialogComponent,
    SmartChoiceGroupListComponent,
    GroupEditDialogComponent,
    SmartChoiceManualPreferencesComponent,
    SmartChoiceDivisionComponent,
    NewSmartChoiceDialogComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatRippleModule,
    MatChipsModule,
    HttpClientModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSlideToggleModule,
    DragDropModule,
    MatBadgeModule,
    ScrollingModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatSliderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

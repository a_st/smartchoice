import {Component, OnInit} from '@angular/core';
import {SmartChoice} from '../shared/models/smart-choice';
import {ActivatedRoute} from '@angular/router';
import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Person} from '../shared/models/person';
import {Group} from '../shared/models/group';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-smart-choice-manual-preferences',
  templateUrl: './smart-choice-manual-preferences.component.html',
  styleUrls: ['./smart-choice-manual-preferences.component.scss']
})
export class SmartChoiceManualPreferencesComponent implements OnInit {

  public smartChoice: SmartChoice;
  public smartChoiceIndex: number;

  public selectedPerson: Person;
  public availableGroups: Group[] = [];

  constructor(private route: ActivatedRoute, private smartChoiceService: SmartChoiceService, private snackBar: MatSnackBar, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.smartChoiceIndex = params.index;
      this.smartChoiceService.getSmartChoices().subscribe(choices => {
        this.smartChoice = choices[this.smartChoiceIndex];
      });
    });
  }

  selectPerson(person: Person) {
    this.selectedPerson = person;

    const allGroups = this.smartChoice.groups;
    this.availableGroups = allGroups.filter(g =>
      person.preferredGroups.findIndex(g1 => g1.uuid === g.uuid) === -1
    );
  }

  drop(event: CdkDragDrop<Group[], any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
  }

  moveGroupToPreferences(index: number) {
    transferArrayItem(this.availableGroups, this.selectedPerson.preferredGroups, index, this.selectedPerson.preferredGroups.length);
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
  }

  moveGroupBackToAvailableGroups(index: number) {
    transferArrayItem(this.selectedPerson.preferredGroups, this.availableGroups, index, this.availableGroups.length);
    this.smartChoiceService.saveSmartChoice(this.smartChoice);
  }
}

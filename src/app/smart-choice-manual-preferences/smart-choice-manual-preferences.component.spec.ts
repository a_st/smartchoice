import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartChoiceManualPreferencesComponent } from './smart-choice-manual-preferences.component';
import {TestingModule} from '../shared/testing/testing.module';

describe('SmartChoiceManualPreferencesComponent', () => {
  let component: SmartChoiceManualPreferencesComponent;
  let fixture: ComponentFixture<SmartChoiceManualPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartChoiceManualPreferencesComponent ],
      imports: [ TestingModule ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartChoiceManualPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

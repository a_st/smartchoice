import {Component, OnInit} from '@angular/core';
import {SmartChoice} from '../shared/models/smart-choice';
import {ActivatedRoute} from '@angular/router';
import {SmartChoiceService} from '../shared/services/smart-choice.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {SmartChoiceDivisionService} from '../shared/services/smart-choice-division.service';
import {DivisionResult} from '../shared/models/division-result';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Person} from '../shared/models/person';
import {FilledGroupService} from '../shared/services/filled-group.service';
import {FilledGroup} from '../shared/models/filled-group';

@Component({
  selector: 'app-smart-choice-division',
  templateUrl: './smart-choice-division.component.html',
  styleUrls: ['./smart-choice-division.component.scss']
})
export class SmartChoiceDivisionComponent implements OnInit {

  private readonly CALCULATION_AMOUNT_MAJOR = 200;
  private readonly CALCULATION_AMOUNT_MINOR = 40;

  public smartChoice: SmartChoice;
  public smartChoiceIndex: number;
  // 0 = paused; 1 = running; 2 = finished
  public calculationStatus = 0;
  public calculationProgress = 0;

  public manualMovementMade = false;

  private bestDivisionResult: DivisionResult;

  constructor(private route: ActivatedRoute, private smartChoiceService: SmartChoiceService, private snackBar: MatSnackBar, private dialog: MatDialog,
              private smartChoiceDivisionService: SmartChoiceDivisionService, private filledGroupService: FilledGroupService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.smartChoiceIndex = params.index;
      this.smartChoiceService.getSmartChoices().subscribe(choices => {
        this.smartChoice = choices[this.smartChoiceIndex];
      });
    });
  }

  start() {
    this.calculationStatus = 1;
    this.calculationProgress = 0;
    this.bestDivisionResult = null;
    setTimeout(() => {
      this.calculateNTimes(this.CALCULATION_AMOUNT_MAJOR);
    }, 0);
  }

  private calculateNTimes(n: number) {
    this.calculationProgress = Math.floor((this.CALCULATION_AMOUNT_MAJOR - n) / this.CALCULATION_AMOUNT_MAJOR * 100);
    const divisionResult = this.smartChoiceDivisionService.divideUsersOntoGroupsRepeat(this.smartChoice, this.CALCULATION_AMOUNT_MINOR);
    if (!this.bestDivisionResult) {
      this.bestDivisionResult = divisionResult;
    } else if (this.bestDivisionResult.satisfaction > divisionResult.satisfaction) {
      // console.log('N: ' + n);
      // console.log(this.diffFilledGroup(divisionResult.filledGroup, this.bestDivisionResult.filledGroup));
      this.bestDivisionResult = divisionResult;
    }
    console.log(this.bestDivisionResult.satisfaction);
    if (n > 0) {
      setTimeout(() => {
        this.calculateNTimes(n - 1);
      }, 0);
    } else {
      this.calculationStatus = 2;
      this.manualMovementMade = false;
      // // TEMP
      // const persons = this.bestDivisionResult.filledGroup
      //   .map(filledGroup => {
      //     return filledGroup.persons
      //       .map(person => {
      //           const index = person.preferredGroups.findIndex(group => group.uuid === filledGroup.group.uuid);
      //           let satisfaction = index;
      //           if (index === -1) {
      //             satisfaction = person.preferredGroups.length;
      //           }
      //           return {person, satisfaction, group: filledGroup.group};
      //         }
      //       );
      //   })
      //   .reduce((previousValue, currentValue) => previousValue.concat(currentValue));
      //
      // persons.sort((a, b) => b.satisfaction - a.satisfaction);
      // console.log(persons);
      // //TEMP END
      // console.log(this.bestDivisionResult);
    }
  }

  //
  // diffFilledGroup(a: FilledGroup[], b: FilledGroup[]) {
  //   a = JSON.parse(JSON.stringify(a));
  //   for (let i = 0; i < a.length; i++) {
  //     a[i].persons = this.diff(a[i].persons, b[i].persons);
  //   }
  //   return a;
  // }
  //
  // diff(a: Person[], b: Person[]) {
  //   return a.filter((i) => b.findIndex(person => person.uuid === i.uuid) < 0 );
  // }

  drop(event: CdkDragDrop<Person[], any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.bestDivisionResult.satisfaction = this.filledGroupService.calculateDissatisfaction(this.bestDivisionResult.filledGroups);
    this.manualMovementMade = true;
  }

  getAllMatListPersonCardIds(): string[] {
    return this.bestDivisionResult.filledGroups.map(filledGroup => 'G_' + filledGroup.group.uuid);
  }

  getFilledGroupsWithCapacityIncreaseNotice(): FilledGroup[] {
    return this.bestDivisionResult.filledGroups.filter(group => group.increaseCapacityNotice > 0);
  }

  increaseCapacityOfFilledGroup(filledGroup: FilledGroup) {
    filledGroup.group.capacity += filledGroup.increaseCapacity;
    filledGroup.increaseCapacityNotice -= filledGroup.increaseCapacity;
    this.snackBar.open('Capacity increased', 'Start division', {duration: 4000, verticalPosition: 'top'}).onAction().subscribe(() => {
      this.start();
    });
  }
}


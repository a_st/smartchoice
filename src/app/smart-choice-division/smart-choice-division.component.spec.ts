import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartChoiceDivisionComponent } from './smart-choice-division.component';
import {TestingModule} from '../shared/testing/testing.module';

describe('SmartChoiceDivisionComponent', () => {
  let component: SmartChoiceDivisionComponent;
  let fixture: ComponentFixture<SmartChoiceDivisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartChoiceDivisionComponent ],
      imports: [ TestingModule ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartChoiceDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
